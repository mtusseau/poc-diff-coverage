TEMPLATE = subdirs

DEFINES += "VERSION=\"\\\"$$VERSION\\\"\""
SUBDIRS += src main

QMAKE_EXTRA_TARGETS += prepare_coverage coverage
QMAKE_CLEAN += *.gcda *.gcno

check.depends += prepare_coverage
coverage.depends = check

CONFIG(testcase){
    SUBDIRS+= test
    SUBDIRS-= main
    LCOVDIR = test/results/lcov/

    prepare_coverage.target = prepare_coverage
    prepare_coverage.commands += rm -rf $$LCOVDIR $$escape_expand(\\n\\t)
    prepare_coverage.commands +=  mkdir -p $$LCOVDIR/html $$escape_expand(\\n\\t)
    prepare_coverage.commands += lcov --quiet --initial --capture --base-directory ./src --directory ./src/ -o $${LCOVDIR}/lcov-init.info $$escape_expand(\\n\\t)
    prepare_coverage.commands += $$escape_expand(\\n)
    prepare_coverage.depends += all

    coverage.target = coverage
    coverage.commands += lcov -q -c -b ./src -d ./src -o $${LCOVDIR}/lcov-run.info --rc lcov_branch_coverage=1 $$escape_expand(\\n\\t)
    #coverage.commands += lcov -q -e $${LCOVDIR}/.lcov.base1 -o $${LCOVDIR}/.lcov.base $$PWD/src/* $$escape_expand(\\n\\t)
    #coverage.commands += lcov -q -e $${LCOVDIR}/.lcov.run1 -o $${LCOVDIR}/.lcov.run $$PWD/src/* $$escape_expand(\\n\\t)
    #coverage.commands += lcov -q -a $${LCOVDIR}/.lcov.base -a $${LCOVDIR}/.lcov.run -o $${LCOVDIR}/.lcov.total $$escape_expand(\\n\\t)
    coverage.commands += genhtml -s -o $${LCOVDIR}/html $${LCOVDIR}/lcov-run.info --rc genhtml_branch_coverage=1
    coverage.commands += $$escape_expand(\\n)
}
