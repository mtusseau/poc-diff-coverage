#ifndef MYMATH_H
#define MYMATH_H

#include <QString>
#include <QObject>

class MyMath
{
    Q_GADGET
public:
    MyMath();
    int abs(int x);
};

#endif // DIFF_H
