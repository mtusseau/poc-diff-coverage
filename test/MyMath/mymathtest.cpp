#include <QString>
#include <QtTest>
#include "mymath.h"

class MyMathTest : public QObject
{
    Q_OBJECT

public:
    MyMathTest();

private Q_SLOTS:
    void testAbs_data();
    void testAbs();
};

MyMathTest::MyMathTest()
{
}

void MyMathTest::testAbs_data()
{
    QTest::addColumn<MyMath*>("math");
    QTest::addColumn<int>("input");
    QTest::addColumn<int>("expected");
    MyMath* m1 = new MyMath();
    QTest::newRow("Positive") << m1 << -1 << 1;
}

void MyMathTest::testAbs()
{
    QFETCH(MyMath*, math);
    QFETCH(int, input);
    QFETCH(int, expected);
    int result = math->abs(input);
    QCOMPARE(result, expected);
}

QTEST_APPLESS_MAIN(MyMathTest)

#include "mymathtest.moc"
